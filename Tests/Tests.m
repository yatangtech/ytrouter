//
//  Tests.m
//  Tests
//
//  Created by LiXiang on 2017/5/12.
//  Copyright © 2017年 Ya Tang. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "YTRouter.h"

@interface Tests : XCTestCase

@property(nonatomic) BOOL testResult;
@property(nonatomic) NSDictionary *params;

@end

@implementation Tests

- (void)setUp {
    [super setUp];
    [[YTRouter defaultRouter] registerURLPattern:@"testA" handler:^void(NSDictionary * _Nullable parameters) {
        self.params = parameters;
    }];
    [[YTRouter defaultRouter] registerURLPattern:@"testB" handler:^void(NSDictionary * _Nullable parameters) {
        self.params = parameters;
    }];
    
    [[YTRouter defaultRouter] registerURLPattern:@"testC" handler:^void(NSDictionary * _Nullable parameters) {
        self.params = parameters;
    }];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    [[YTRouter defaultRouter] unRegisterAllURLPattern];
}

- (void)testRoutesSuccessful {
    self.testResult = [YTRouter routeToPath:@"app://1.0/testA"];
    [self expectSuccess];
    
    self.testResult = [YTRouter routeToPath:@"app://1.0/testB"];
    [self expectSuccess];

}

- (void)testRoutesSuccessfulWithParamters {
    self.testResult = [YTRouter routeToPath:@"app://1.0/testA?p1=0&p2=1"];
    [self expectSuccessWithParams];
    
    XCTAssertEqualObjects(self.params[@"p1"], @"0");
    XCTAssertEqualObjects(self.params[@"p2"], @"1");
    
    self.testResult = [YTRouter routeToPath:@"app://1.0/testB?p1=0" params:@{@"p2": @"1", @"p3": @"2"}];
    XCTAssertEqualObjects(self.params[@"p1"], @"0");
    XCTAssertEqualObjects(self.params[@"p2"], @"1");
    XCTAssertEqualObjects(self.params[@"p3"], @"2");
}

- (void)testRoutesFail {
    self.testResult = [YTRouter routeToPath:@"app://1.0/aaaaa"];
    [self expectFail];
    
    self.testResult = [YTRouter routeToPath:@"app"];
    [self expectFail];
    
    self.testResult = [YTRouter routeToPath:@"app://1.0"];
    [self expectFail];
    
    self.testResult = [YTRouter routeToPath:@"app://test"];
    [self expectFail];
    
    self.testResult = [YTRouter routeToPath:@"app://aaa/test"];
    [self expectFail];
}

- (void)testRouteWithConfig {
    [[YTRouter defaultRouter] configuration:@"xcr" currentVersion:@(2.0)];
    
    self.testResult = [YTRouter routeToPath:@"xcr://1.0/testA"];
    [self expectSuccess];
    
    self.testResult = [YTRouter routeToPath:@"xcr://2.0/testA"];
    [self expectSuccess];
    
    self.testResult = [YTRouter routeToPath:@"app://1.0/testA"];
    [self expectFail];
    
    self.testResult = [YTRouter routeToPath:@"xcr://3.0/testA"];
    [self expectFail];
}

- (void)expectSuccess {
    XCTAssertTrue(self.testResult, @"Expected any route Successful");
}

- (void)expectFail {
    XCTAssertFalse(self.testResult, @"Expected none of routes failed");
}

- (void)expectSuccessWithParams {
    XCTAssertTrue(self.testResult, @"Expected any route Successful");
    XCTAssertNotNil(self.params, @"Expected route with parameters");
}

@end
