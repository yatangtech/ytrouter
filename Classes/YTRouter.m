//
//  YTRouter.m
//  YTRouter
//
//  Created by LiXiang on 2017/5/12.
//  Copyright © 2017年 Ya Tang. All rights reserved.
//

#import "YTRouter.h"

@interface YTRouter ()

@property(nonatomic) NSString *scheme;
@property(nonatomic) NSNumber *currentVersion;
@property(nonatomic) NSMutableDictionary *routesDic;

@end

@implementation YTRouter

+ (instancetype)defaultRouter {
    static YTRouter *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[YTRouter alloc] init];
    });
    return instance;
}

- (void)configuration:(NSString *)scheme currentVersion:(NSNumber *)version {
    self.scheme = scheme;
    self.currentVersion = version;
}

- (void)registerURLPattern:(NSString *)pattern handler:(YTRouterHandler)handler {
    NSParameterAssert(pattern);
    if (!self.routesDic) {
        self.routesDic = [NSMutableDictionary dictionary];
    }
    if ([self.routesDic objectForKey:pattern]) {
        NSAssert(NO, @"已经注册过%@", pattern);
    } else {
        [self.routesDic setObject:[handler copy] forKey:pattern];
    }
}

- (void)unRegisterAllURLPattern {
    [self.routesDic removeAllObjects];
}

- (BOOL)routeToURL:(NSURL *)url {
    return [self routeToURL:url params:nil];
}

- (BOOL)routeToURL:(NSURL *)url params:(NSDictionary *)params {
    NSParameterAssert(url);
    NSString *scheme = url.scheme;
    if (self.scheme) {
        if (![self.scheme isEqualToString:scheme]) {
            return NO;
        }
    }
    NSString *version = url.host;
    NSString *path = [url.path stringByReplacingOccurrencesOfString:@"/" withString:@""];
    NSString *query = url.query;
    NSLog(@"scheme: %@", scheme);
    NSLog(@"path: %@", path);
    NSLog(@"query: %@", query);
    
    BOOL isValid = [self checkVersion:version withCurrentVersion:self.currentVersion];
    if (isValid) {
        YTRouterHandler handler = self.routesDic[path];
        if (handler) {
            if (query) {
                NSMutableDictionary *queryParams = [[self parseQueryParams:query] mutableCopy];
                if (queryParams) {
                    [queryParams addEntriesFromDictionary:params];
                    handler(queryParams);
                    return YES;
                }
            }
            handler(params);
            return YES;
        } else {
            return NO;
        }
    } else {
        return NO;
    }
}

+ (BOOL)routeToPath:(NSString *)path {
    return [self routeToPath:path params:nil];
}

+ (BOOL)routeToPath:(NSString *)path params:(NSDictionary *)params {
    return [[self defaultRouter] routeToPath:path params:params];
}

- (BOOL)routeToPath:(NSString *)path {
    NSURL *url = [NSURL URLWithString:path];
    return [self routeToURL:url];
}

- (BOOL)routeToPath:(NSString *)path params:(NSDictionary *)params {
    NSURL *url = [NSURL URLWithString:path];
    return [self routeToURL:url params:params];
}

- (BOOL)checkVersion:(NSString *)versionString withCurrentVersion:(NSNumber *)currentVersion {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    NSNumber *version = [formatter numberFromString:versionString];
    if (!version) {
        return NO;
    }
    if (currentVersion) {
        if ([version compare:currentVersion] == NSOrderedDescending) {
            return NO;
        } else {
            return YES;
        }
    } else {
        return YES;
    }
}

- (NSDictionary *)parseQueryParams:(NSString *)queryString {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    NSArray *comps = [queryString componentsSeparatedByString:@"&"];
    for (NSString *comp in comps) {
        NSArray *paramPair = [comp componentsSeparatedByString:@"="];
        [dic setObject:paramPair[1] forKey:paramPair[0]];
    }
    return dic;
}

@end
