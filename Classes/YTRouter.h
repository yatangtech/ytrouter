//
//  YTRouter.h
//  YTRouter
//
//  Created by LiXiang on 2017/5/12.
//  Copyright © 2017年 Ya Tang. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^YTRouterHandler)(NSDictionary * _Nullable parameters);

@protocol YTRoutable <NSObject>

- (void)routingWithParameters:(NSDictionary *)parameters;

@end

@interface YTRouter : NSObject

+ (instancetype)defaultRouter;

- (void)configuration:(NSString *)scheme currentVersion:(NSNumber *)version;

- (void)registerURLPattern:(NSString *)pattern handler:(YTRouterHandler)handler;

- (void)unRegisterAllURLPattern;

+ (BOOL)routeToPath:(NSString *)path;

+ (BOOL)routeToPath:(NSString *)path params:(NSDictionary * _Nullable)params;

@end

NS_ASSUME_NONNULL_END
